package com.example.yf.customseekbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.yf.customseekbar.widiget.BloodSeekBar;

public class MainActivity extends AppCompatActivity {
    BloodSeekBar seekBar;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar = findViewById(R.id.blood_seek);
        button = findViewById(R.id.move_seek);
        button.setOnClickListener(new View.OnClickListener() {
            private int i = 1;

            @Override
            public void onClick(View v) {
//                i+=i+20;
                seekBar.setBublePos(110 , 110);

            }
        });
    }
}
