package com.example.yf.customseekbar.widiget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;

import com.example.yf.customseekbar.R;


/**
 * Created by YinFeng on 2019/8/7.
 */
@SuppressLint("NewApi")
public class BloodSeekBar extends View {

    private static final String TAG = "BloodSeekBar";
    private final BubbleView mBubbleView;
    private int mTrackSize;
    private float mSectionTextSize;
    private String[] mSectionTextArray = {"低", "高"};
    private CharSequence[] mSectionTextC;
    private Paint mPaint;
    private Paint RectPaint;
    private int mSectionTextColor;
    private int mLeft, mRight, textW;
    private float mSectionOffset;
    private TextPaint textPaint;
    private StaticLayout.Builder builder;
    private StaticLayout staticLayout;//绘制可换行的文字

    private LinearGradient linearGradient;
    private int lySpace;//渐变宽度
    private int[] colors;
    private int mSecondTrackSize;
    private int mSecondTrackColor;
    private int mTrackLength;
    private int mThumbCenterX;
    private int mThumbRadius;
    private int mThumbRadiusOnDragging;

    private RectF rightRect;

    private int mBubbleRadius;
    private float mBubbleTextSize;
    private int mBubbleTextColor;
    private int mBubbleColor;
    private int h, w;
    private WindowManager mWindowManager;
    private float mBubbleCenterRawSolidX;
    private float mBubbleCenterRawSolidY;
    private float mBubbleCenterRawX;
    private WindowManager.LayoutParams mLayoutParams;
    private int[] mPoint = new int[2];
    private boolean isTouchToSeekAnimEnd = true;
    private float mPreSecValue; // previous SectionValue
    private Rect mRectText;
    private float[][] sbpArr = new float[][]{{0, 89}, {90, 119}, {120, 139},
            {140, 159}, {160, 179}, {180, 300}, {400, 500}};//舒张压的判断范围
    private float[][] dbpArr = new float[][]{{0, 59}, {60, 79},
            {80, 89}, {90, 99}, {100, 109}, {110, 300}, {400, 500}};//收缩压
    private float[][] sbpDisArr, dbpDisArr;//每一段距离的最小单位
    private int pos = 3;
    private int sbp = 110, dbp = 110;//sbp 收缩压。dbp 舒张压

    private int min = 0;
    private int max = 300;


    public BloodSeekBar(Context context) {
        this(context, null);
    }

    public BloodSeekBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public BloodSeekBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //设置相关属性
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BloodSeekBar, defStyleAttr, 0);
        mSectionTextArray = context.getResources().getStringArray(R.array.seek_section);
//        mSectionTextC = ta.getTextArray(R.styleable.BloodSeekBar_bsb_track_size);
//if(mSectionTextC!=null){
//    for (int j = 0; j < mSectionTextC.length; j++) {
//        Log.i(TAG, "BloodSeekBar: mSectionTextC " + mSectionTextC[j]);
//    }
//}else {
//    Log.i(TAG, "BloodSeekBar: mSectionTextC ==null "  );
//}

        sbpDisArr = new float[mSectionTextArray.length][2];
        dbpDisArr = new float[mSectionTextArray.length][2];
        mSectionTextSize = ta.getDimensionPixelSize(R.styleable.BloodSeekBar_bsb_section_text_size, sp2px(10));
        mSectionTextColor = ta.getColor(R.styleable.BloodSeekBar_bsb_section_text_color,
                ContextCompat.getColor(context, R.color.colorAccent));

        mTrackSize = ta.getDimensionPixelSize(R.styleable.BloodSeekBar_bsb_track_size, dp2px(20));
        mSecondTrackColor = ta.getColor(R.styleable.BloodSeekBar_bsb_second_track_color,
                ContextCompat.getColor(context, R.color.colorPrimary));
        mThumbRadius = ta.getDimensionPixelSize(R.styleable.BloodSeekBar_bsb_thumb_radius, mTrackSize / 2 + dp2px(4));
        mThumbRadiusOnDragging = ta.getDimensionPixelSize(R.styleable.BloodSeekBar_bsb_thumb_radius_on_dragging,
                mThumbRadius + dp2px(2));

        mBubbleRadius = dp2px(10);
        mBubbleColor = ta.getColor(R.styleable.BloodSeekBar_bsb_bubble_color, mSecondTrackColor);
        mBubbleTextSize = ta.getDimensionPixelSize(R.styleable.BloodSeekBar_bsb_bubble_text_size, sp2px(14));
        mBubbleTextColor = ta.getColor(R.styleable.BloodSeekBar_bsb_bubble_text_color, Color.WHITE);

        String colorStr = ta.getString(R.styleable.BloodSeekBar_bsb_colors);
        if (colorStr == null || colorStr.equals("")) {
            colorStr = "#ff46e880_#ffd44720_#ffe1992e_#ffd44720_#ffdddddd_#ffcccccc_#ffe1992e";
        }
        parseStringToLayerColors(colorStr);
        ta.recycle();

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setTextAlign(Paint.Align.CENTER);

        RectPaint = new Paint();

        mRectText = new Rect();
        textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setAntiAlias(true);
        textPaint.setStrokeCap(Paint.Cap.ROUND);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(dp2px(12));
        textPaint.setColor(mSectionTextColor);


        rightRect = new RectF();

        if (mSecondTrackSize < mTrackSize) {
            mSecondTrackSize = mTrackSize + dp2px(5);
        }

        mBubbleView = new BubbleView(context);
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        // init BubbleView

        mLayoutParams = new WindowManager.LayoutParams();
        mLayoutParams.gravity = Gravity.START | Gravity.TOP;
        mLayoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        mLayoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        mLayoutParams.format = PixelFormat.TRANSLUCENT;
        mLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
        // MIUI禁止了开发者使用TYPE_TOAST，Android 7.1.1 对TYPE_TOAST的使用更严格
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            mLayoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION;
        } else {
            mLayoutParams.type = WindowManager.LayoutParams.TYPE_TOAST;
        }


    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        locatePositionOnScreen();
//        mBubbleView.layout(getLeft() + posX, getTop(), getRight(), getBottom());
    }

    int i = 0;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        h = heightMeasureSpec;
        w = widthMeasureSpec;
        lySpace = getMeasuredWidth();
        sweepGradientInit();

        mLeft = getPaddingLeft();
        mRight = getMeasuredWidth() - getPaddingRight();
        i = i + 1;
        mSectionOffset = (mSectionTextArray == null) ? 0 : (mRight - mLeft) * 1f / mSectionTextArray.length;
        textW = (mSectionTextArray == null) ? 0 : (mRight - mLeft) / mSectionTextArray.length;
        mTrackLength = mRight - mLeft;

        mBubbleView.measure(widthMeasureSpec, heightMeasureSpec);
        for (int i = 0; i < mSectionTextArray.length; i++) {//计算文字需要的最大高度
            builder = StaticLayout.Builder.obtain(mSectionTextArray[i], 0, mSectionTextArray[i].length(), textPaint, textW)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL) //网上直接是对象的方式不好会导致内存一直回收，建议用建造者模式
                    .setLineSpacing(0, 1)
                    .setIncludePad(true);
            h = Math.max(h, builder.build().getHeight());
        }
        h = h + mBubbleView.getMeasuredHeight() + mSecondTrackSize;
        setMeasuredDimension(getMeasuredWidth(), h);

    }

    public void setBublePos(int sbp, int dbp) {
        if (sbp >= 140 && dbp < 90) {//属于最后一种情况
            this.sbp = 450;
            this.dbp = 450;
            return;
        }
        this.sbp = sbp;
        this.dbp = dbp;

    }

    @Override
    public void postInvalidate() {
        super.postInvalidate();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setLayerType(LAYER_TYPE_SOFTWARE, null);//对单独的View在运行时阶段禁用硬件加速
        float xLeft = getPaddingLeft();
        float xRight = getMeasuredWidth() - getPaddingRight();
        float yTop = getPaddingTop() + mBubbleView.getMeasuredHeight();

        // draw track
//        mPaint.setShader(linearGradient);//渐变颜色
        mPaint.setColor(mSecondTrackColor);
//        mPaint.setStrokeWidth(mSecondTrackSize);
        mPaint.setStrokeCap(Paint.Cap.BUTT);
        float xl;
        float s = (getMeasuredWidth() - getPaddingRight() - getPaddingLeft()) * 1f / mSectionTextArray.length;//每一段的长度
        for (int k = 0; k < colors.length; k++) {
            xl = getPaddingLeft() + k * s;
            mPaint.setColor(colors[k]);
            float f1 = sbpArr[k][1] - sbpArr[k][0], f2 = dbpArr[k][1] - dbpArr[k][0];
            float f = f1 * 1f / s, f3 = f2 * 1f / s;
            sbpDisArr[k][0] = xl;
            sbpDisArr[k][1] = f;
            dbpDisArr[k][0] = xl;
            dbpDisArr[k][1] = f3;

            if (k == 0) {
                RectPaint.setColor(colors[1]);
//                canvas.drawArc(xl,getPaddingTop()+dp2px(2),xl + mSecondTrackSize,rightRect.right,-270f,180f,false,mPaint);
//                xl = xl + mSecondTrackSize / 2;
                rightRect.left = xl;
                rightRect.right = xl + mSecondTrackSize;
                rightRect.top = yTop;
                rightRect.bottom = yTop + mSecondTrackSize;
                canvas.save();
//                canvas.clipRect(rightRect);
//                canvas.translate(mSecondTrackSize / 2, 0);
                canvas.drawRect(xl + mSecondTrackSize / 2, yTop, xl + s, yTop + mSecondTrackSize, mPaint);
                canvas.restore();
                canvas.drawArc(rightRect, -270, 180, false, mPaint);

                continue;
            }
            if (k == colors.length - 1) {
                RectPaint.setColor(colors[0]);
                rightRect.left = xl;
                rightRect.right = xl + mSecondTrackSize;
                rightRect.top = yTop;
                rightRect.bottom = yTop + mSecondTrackSize;
                canvas.drawRect(xl, yTop, xl + s - mSecondTrackSize / 2, yTop + mSecondTrackSize, mPaint);
                canvas.save();
//                canvas.clipRect(rightRect);
                canvas.translate(s - mSecondTrackSize, 0);
                canvas.drawArc(rightRect, -90, 180, false, mPaint);
                canvas.restore();

                continue;
            }
            canvas.drawRect(xl, yTop, xl + s, yTop + mSecondTrackSize, mPaint);

        }
//        mPaint.setShader(null);//关闭渐变颜色

//        Log.i("1", "onDraw: i="+);
        drawSection(canvas);//绘制底部指示标签
        mBubbleView.draw(canvas);
    }

    public void sweepGradientInit() {
        //渐变颜色.colors和pos的个数一定要相等

        float[] pos = new float[colors.length];
        for (int i = 0; i < pos.length; i++) {
            pos[i] = (i % pos.length) * 0.1f;
        }
//        float[] pos = {0f, 0.5f, 1.0f};
        //Shader.TileMode.MIRROR 表示最后剩下的空间用最后的颜色表示
        linearGradient = new LinearGradient(0, 0, lySpace / 2, lySpace / 2, colors, null, Shader.TileMode.MIRROR);
        Matrix matrix = new Matrix();
        linearGradient.setLocalMatrix(matrix);
    }

    private void parseStringToLayerColors(String colorStr) {
        String[] colorArray = colorStr.split("_");
        colors = new int[colorArray.length];
        for (int i = 0; i < colorArray.length; i++) {
            try {
                colors[i] = Color.parseColor(colorArray[i]);
            } catch (IllegalArgumentException ex) {
                throw new IllegalArgumentException("色值解析失败 | " + ex.getLocalizedMessage());
            }
        }
    }

    private void locatePositionOnScreen() {
        getLocationOnScreen(mPoint);

        ViewParent parent = getParent();
        if (parent != null && parent instanceof View && ((View) parent).getMeasuredWidth() > 0) {
            mPoint[0] %= ((View) parent).getMeasuredWidth();
        }
        mBubbleCenterRawSolidX = mPoint[0] + mLeft - mBubbleView.getMeasuredWidth() / 2f;

        mBubbleCenterRawX = mBubbleCenterRawSolidX + mTrackLength * 2 / 7;
        mBubbleCenterRawSolidY = mPoint[1] - mBubbleView.getMeasuredHeight();
        mBubbleCenterRawSolidY -= dp2px(24);


        Context context = getContext();
        if (context instanceof Activity) {
            Window window = ((Activity) context).getWindow();
            if (window != null) {
                int flags = window.getAttributes().flags;
                if ((flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0) {
                    Resources res = Resources.getSystem();
                    int id = res.getIdentifier("status_bar_height", "dimen", "android");
                    mBubbleCenterRawSolidY += res.getDimensionPixelSize(id);
                }
            }
        }
    }

    private void drawSection(Canvas canvas) {

        float xLeft = getPaddingLeft() + dp2px(20);
        float xRight = getMeasuredWidth() - getPaddingRight();
        float yTop = getPaddingTop() + mBubbleView.getMeasuredHeight() + mSecondTrackSize;
        float x_;
        float y_ = yTop;
        float r;
        for (int i = 0; i < mSectionTextArray.length; i++) {
            x_ = xLeft + i * mSectionOffset;

            canvas.save();
            canvas.translate(x_, y_);
            builder = StaticLayout.Builder.obtain(mSectionTextArray[i], 0, mSectionTextArray[i].length(), textPaint, textW)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL) //网上直接是对象的方式不好会导致内存一直回收，建议用建造者模式
                    .setLineSpacing(0, 1)
                    .setIncludePad(true);
            builder.build().draw(canvas);
//            canvas.translate(-staticLayout.getWidth() / 2, -staticLayout.getHeight() / 2);

            canvas.restore();
        }
    }

    void intArr() {
        sbpArr = new float[mSectionTextArray.length][2];

        for (int i = 0; i < mSectionTextArray.length; i++) {
//sbpArr
        }
    }

    static int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                Resources.getSystem().getDisplayMetrics());
    }

    static int sp2px(int sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp,
                Resources.getSystem().getDisplayMetrics());
    }

    private class BubbleView extends View {

        private Paint mBubblePaint;
        private Path mBubblePath;
        private RectF mBubbleRectF;
        private Rect mRect;
        private String mProgressText = "";

        BubbleView(Context context) {
            this(context, null);
        }

        BubbleView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        BubbleView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);

            mBubblePaint = new Paint();
            mBubblePaint.setAntiAlias(true);
            mBubblePaint.setTextAlign(Paint.Align.CENTER);
            mBubblePaint.setTextSize(dp2px(10));
            mBubblePath = new Path();
            mBubbleRectF = new RectF();
            mRect = new Rect();
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

            setMeasuredDimension(3 * mBubbleRadius, 3 * mBubbleRadius);

            mBubbleRectF.set(getMeasuredWidth() / 2f - mBubbleRadius, 0,
                    getMeasuredWidth() / 2f + mBubbleRadius, 2 * mBubbleRadius);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            Log.i(TAG, "onDraw: iii");
            mBubblePath.reset();
            float x0 = getMeasuredWidth() / 2f;
            float y0 = getMeasuredHeight() - mBubbleRadius / 3f;
//            float f = calPos();//根据血压值计算对应位置
            float f;
            f = calPos();
            if (sbp < 0 || dbp < 0) return;
//            f = calTransPos();
            if (f < 0) return;
            Log.i(TAG, "onDraw: f==" + f);
            canvas.save();

            canvas.translate(f, 0);
            mBubblePath.moveTo(x0, y0);
            float x1 = (float) (getMeasuredWidth() / 2f - Math.sqrt(3) / 2f * mBubbleRadius);
            float y1 = 3 / 2f * mBubbleRadius;
            mBubblePath.quadTo(x1 - dp2px(2), y1 - dp2px(2), x1, y1);
            mBubblePath.arcTo(mBubbleRectF, 150, 240);

            float x2 = (float) (getMeasuredWidth() / 2f + Math.sqrt(3) / 2f * mBubbleRadius);
            mBubblePath.quadTo(x2 + dp2px(2), y1 - dp2px(2), x0, y0);
            mBubblePath.close();

            mBubblePaint.setColor(mBubbleColor);
            canvas.drawPath(mBubblePath, mBubblePaint);

            mBubblePaint.setTextSize(mBubbleTextSize);
            mBubblePaint.setColor(mBubbleTextColor);
            mBubblePaint.getTextBounds(mProgressText, 0, mProgressText.length(), mRect);
            Paint.FontMetrics fm = mBubblePaint.getFontMetrics();
            float baseline = mBubbleRadius + (fm.descent - fm.ascent) / 2f - fm.descent;
            canvas.drawText(mProgressText, getMeasuredWidth() / 2f, baseline, mBubblePaint);
            canvas.restore();
        }

        private float calPos() {
            int sbpLevel, dbpLevel;
            sbpLevel = calSbpLevel(sbp);
            dbpLevel = calDbpLevel(dbp);
            float f;
            if (sbpLevel == dbpLevel) {
                if (sbpLevel == sbpArr.length - 1) {//表示是最后一项的范围
                    f = sbpDisArr[sbpLevel][0];
                    return f;
                }
                f = sbpDisArr[sbpLevel][0] + (sbp - sbpArr[sbpLevel][0]) * sbpDisArr[sbpLevel][1];
                return f;
            }
//            if (sbpLevel == 1 || sbpLevel == 2) {//收缩压正常 以舒张压为准
//                f = dbpDisArr[dbpLevel][0] + (dbp - sbpArr[dbpLevel][0]) * dbpDisArr[dbpLevel][1];
//                return f;
//            }
//            if (dbpLevel == 1 || dbpLevel == 2) {//舒张压正常 以收缩压为准
//                f = sbpDisArr[sbpLevel][0] + (sbp - sbpArr[sbpLevel][0]) * sbpDisArr[sbpLevel][1];
//                return f;
//            }
            //先判断正常 然后判断正常高值
            if (sbpLevel == 1) {//收缩压正常 以舒张压为准
                f = dbpDisArr[dbpLevel][0] + (dbp - dbpArr[dbpLevel][0]) * dbpDisArr[dbpLevel][1];
                return f;
            }
            if (dbpLevel == 1) {//舒张压正常 以收缩压为准
                f = sbpDisArr[sbpLevel][0] + (sbp - sbpArr[sbpLevel][0]) * sbpDisArr[sbpLevel][1];
                return f;
            }
            if (sbpLevel == 2) {//收缩压正常 以舒张压为准
                f = dbpDisArr[dbpLevel][0] + (dbp - dbpArr[dbpLevel][0]) * dbpDisArr[dbpLevel][1];
                return f;
            }
            if (dbpLevel == 2) {//舒张压正常 以收缩压为准
                f = sbpDisArr[sbpLevel][0] + (sbp - sbpArr[sbpLevel][0]) * sbpDisArr[sbpLevel][1];
                return f;
            }
            return -1;
        }

        private int calSbpLevel(int sbp) {
            int pos = 0;
            int i = 0;
            for (int k = 0; k < sbpArr.length; k++) {
                i = i + 1;
                if (sbp < sbpArr[0][0]) {//都小于0，直接取0
                    pos = 0;
                    break;
                }
                if (sbp < sbpArr[k][0]) {// >0并且小于区间的最小值 sbp * sbpDisArr[k][1] 表示 在该区间的百分比
                    pos = k;
                    break;
                }
                if ((sbp >= sbpArr[k][0] && sbp <= sbpArr[k][1])) {
                    pos = k;
                    break;
                }
                if (sbp > sbpArr[k][1] && k == sbpArr.length - 1) {//表示是最后一项的范围
                    pos = k;
                    break;
                }
            }
            return pos;
        }

        private int calDbpLevel(int dbp) {
            int pos = 0;
            int i = 0;
            for (int k = 0; k < dbpArr.length; k++) {
                i = i + 1;
                if (dbp < dbpArr[0][0]) {//都小于0，直接取0
                    pos = 0;
                    break;
                }
                if (dbp < dbpArr[k][0]) {// >0并且小于区间的最小值 sbp * sbpDisArr[k][1] 表示 在该区间的百分比
                    pos = k;
                    break;
                }
                if ((dbp >= dbpArr[k][0] && dbp <= dbpArr[k][1])) {
                    pos = k;
                    break;
                }
                if (dbp > dbpArr[k][1] && k == dbpArr.length - 1) {//表示是最后一项的范围
                    pos = k;
                    break;
                }
            }
            return pos;
        }

        private float calTransPos() {
            //{0, 89}, {90, 119}, {120, 139}, {140, 159}, {160, 179}, {180, 300}, {140, 90}}
            float f = 0, df, d12, d21, d22;
            int i = 0;
            for (int k = 0; k < sbpArr.length; k++) {
                i = i + 1;
                if (sbp < sbpArr[0][0]) {//都小于0，直接取0
                    f = sbpDisArr[0][0];
                    break;
                }

                if (sbp < sbpArr[k][0]) {// >0并且小于区间的最小值 sbp * sbpDisArr[k][1] 表示 在该区间的百分比
                    f = sbpDisArr[k][0] + sbp * sbpDisArr[k][1];
                    return f;
                }
                if ((sbp >= sbpArr[k][0] && sbp <= sbpArr[k][1])) {
                    f = sbpDisArr[k][0] + (sbp - sbpArr[k][0]) * sbpDisArr[k][1];
                    return f;
                }
                if (sbp > sbpArr[k][1] && k == sbpArr.length - 1) {//表示是最后一项的范围
                    f = sbpDisArr[k][0];
                    return f;
                }
            }
            return f;
        }

        @Override
        public void layout(int l, int t, int r, int b) {

            super.layout(l, t, r, b);

        }

    }
}
